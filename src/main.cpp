#include <Arduino.h>

#include <TaskManager.h>
#include <BluetoothRead.h>
#include <BluetoothGlobal.hpp>

void blink(int led, unsigned int delayTime, unsigned int cycles)
{
    for (unsigned int i = 0; i < cycles; ++i) {
        digitalWrite(led, HIGH);
        delay(delayTime);
        digitalWrite(led, LOW);
        delay(delayTime);
    }
    digitalWrite(led, LOW);
}

void setup()
{
    pinMode(LED_BUILTIN, OUTPUT);

    delay(10000);
    // TODO: maybe remove
    Serial.begin(115200);

    blink(LED_BUILTIN, 1000, 5);

    // TODO: make init method instead call "writeData"
    bluetooth::BluetoothGlobal::getInstance().writeData("Init bluetooth");
    task_manager::TaskManager::getInstance().addServiceTask(std::make_unique<BluetoothRead>());

    blink(LED_BUILTIN, 100, 10);
}

void loop()
{
    task_manager::TaskManager::getInstance().executeService();
    task_manager::TaskManager::getInstance().execute();
}
