#pragma once

namespace json {

constexpr char command[] = "command";
constexpr char command_input[] = "command_input";
constexpr char count_of_execute[] = "count_of_execute";

}
