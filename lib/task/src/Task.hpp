#pragma once

#include <ArduinoJson.h>

namespace
{

template<typename T>
inline void setTemplate(const T &newValue, T &memberOfClass)
{
    if (newValue == memberOfClass) {
        return;
    }

    memberOfClass = newValue;
}

}

enum CountOfExecute
{
    Null = -2,
    Forever = -1,
    Zero = 0,
    One = 1,
};

enum class TaskType
{
    Unknown,
    Service,
    Normal,
};

struct TaskInput
{
    virtual ~TaskInput() = default;

    virtual bool fillData(ArduinoJson::JsonObjectConst json)
    {
        return false;
    }
};

class Task
{
public:
    Task(
        uint32_t id,
        bool isEnabled,
        TaskType taskType,
        CountOfExecute countOfExecute = CountOfExecute::Null,
        TaskInput *taskInput = nullptr
    )
        : m_taskInput(taskInput)
        , m_countOfExecute(countOfExecute)
        , m_taskType(taskType)
        , m_id(id)
        , m_isEnabled(isEnabled)
    {
    }

    virtual ~Task()
    {
        delete m_taskInput;
    }

    virtual void run()
    {
    }

    void operator()()
    {
        run();
    }

    void operator--()
    {
        m_countOfExecute = CountOfExecute(m_countOfExecute - 1);
    }

    CountOfExecute countOfExecute() const
    {
        return m_countOfExecute;
    }

    TaskType taskType() const
    {
        return m_taskType;
    }

    uint32_t id() const
    {
        return m_id;
    }

    bool isEnabled() const
    {
        return m_isEnabled;
    }

    void setCountOfExecute(CountOfExecute countOfExecute)
    {
        setTemplate(countOfExecute, m_countOfExecute);
    }

    void setTaskType(TaskType taskType)
    {
        setTemplate(taskType, m_taskType);
    }

    void setId(uint32_t id)
    {
        setTemplate(id, m_id);
    }

    void setIsEnabled(bool isEnabled)
    {
        setTemplate(isEnabled, m_isEnabled);
    }

    void setTaskInput(TaskInput *taskInput)
    {
        delete m_taskInput;
        m_taskInput = taskInput;
    }

    template<typename T>
    T *castAndGetTaskInput()
    {
        return static_cast<T *>(m_taskInput);
    }

protected:
    TaskInput *m_taskInput = nullptr;
    CountOfExecute m_countOfExecute = CountOfExecute::Null;
    TaskType m_taskType = TaskType::Unknown;
    uint32_t m_id;
    bool m_isEnabled;
};
