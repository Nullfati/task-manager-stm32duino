#include "TaskBuilder.h"

#include "BluetoothRead.h"

// TODO: include this files only in debug
#include <SetValueInEeprom.h>
#include <ReadValueFromEeprom.h>

namespace convertors
{

Task *TaskBuilder::createTaskFromStr(const std::string &str, CountOfExecute countOfExecute, TaskInput *taskInput)
{
    if (str == "BluetoothRead") return new BluetoothRead(countOfExecute);
    else if (str == "SetValueInEeprom") return new SetValueInEeprom(countOfExecute, taskInput);
    else if (str == "ReadValueFromEeprom") return new ReadValueFromEeprom(countOfExecute, taskInput);

    return nullptr;
}

TaskInput *TaskBuilder::createTaskInput(
    const std::string &str,
    const ArduinoJson::JsonObjectConst &json
)
{
    if (str == "BluetoothRead") {
        return nullptr;
    }
    else if (str == "SetValueInEeprom") {
        auto taskInput = new SetValueInEepromInput();
        taskInput->fillData(json);
        return taskInput;
    }
    else if (str == "ReadValueFromEeprom") {
        auto taskInput = new ReadValueFromEepromInput();
        taskInput->fillData(json);
        return taskInput;
    }

    return nullptr;
}

}
