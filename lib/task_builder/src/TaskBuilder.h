#pragma once

#include <string>

#include <ArduinoJson.h>

#include <Task.hpp>

namespace convertors {

class TaskBuilder
{
public:
	static Task* createTaskFromStr(
		const std::string &str,
		CountOfExecute countOfExecute,
		TaskInput *taskInput = nullptr
	);

	static TaskInput* createTaskInput(
		const std::string &str,
		const ArduinoJson::JsonObjectConst &json
	);
};


}
