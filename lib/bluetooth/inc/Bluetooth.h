#pragma once

#include <memory>

#include "BluetoothImpl.h"
#include "BluetoothTypes.h"

namespace bluetooth
{

class Bluetooth
{
public:
    explicit Bluetooth(std::unique_ptr<bluetooth_impl::BluetoothImpl> bluetoothImpl)
        : m_bluetoothImpl(std::move(bluetoothImpl))
    {}

    BluetoothResult readData(size_t sizeOfJson)
    {
        return m_bluetoothImpl->readData(sizeOfJson);
    }

    void writeData(const String &data)
    {
        m_bluetoothImpl->writeData(data);
    }
    void writeData(ArduinoJson::JsonObjectConst jsonObject)
    {
        m_bluetoothImpl->writeData(jsonObject);
    }
    void writeData(const ArduinoJson::JsonDocument &jsonDocument)
    {
        m_bluetoothImpl->writeData(jsonDocument);
    }

private:
    std::unique_ptr<bluetooth_impl::BluetoothImpl> m_bluetoothImpl;
};

}
