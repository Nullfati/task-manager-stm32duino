#include "BluetoothImpl.h"

namespace bluetooth_impl
{

BluetoothSerial SerialBT;

BluetoothImpl::BluetoothImpl()
{
    // TODO: add names for bluetooth
    SerialBT.begin("Lolin32Nullfati");
    SerialBT.flush();
    // TODO: print it only in debug
    SerialBT.println("Begin Serial");
    SerialBT.println("Serial TX buffer size " + String(SERIAL_TX_BUFFER_SIZE));
    SerialBT.println("Serial RX buffer size " + String(SERIAL_RX_BUFFER_SIZE));
    SerialBT.flush();
}

bluetooth::BluetoothResult BluetoothImpl::readData(size_t sizeOfJson)
{
    using namespace bluetooth;
    using namespace ArduinoJson;

    BluetoothResult result(sizeOfJson);

    if (SerialBT.available() <= 0) {
        result.error = BluetoothError::NoDataAvailable;
        return result;
    }

    String inputString = SerialBT.readStringUntil('\n');

    bool isStringEmpty = inputString.length() == 0;
    if (isStringEmpty) {
        result.error = BluetoothError::NoDataAvailable;
        return result;
    }

    result.deserializationError = deserializeJson(result.data, inputString);

    if (result.deserializationError != ArduinoJson::DeserializationError::Code::Ok) {
        result.error = BluetoothError::DeserializationError;
        return result;
    }

    result.error = BluetoothError::NoError;
    SerialBT.flush();
    return result;
}

void BluetoothImpl::writeData(const String &data)
{
    if (data.length() != 0) {
        SerialBT.println(data);
    }
    SerialBT.flush();
}

void BluetoothImpl::writeData(ArduinoJson::JsonObjectConst jsonObject)
{
    ArduinoJson::serializeJson(jsonObject, SerialBT);
    SerialBT.flush();
}

void BluetoothImpl::writeData(const ArduinoJson::JsonDocument &jsonDocument)
{
    ArduinoJson::serializeJson(jsonDocument, SerialBT);
    SerialBT.flush();
}

}
