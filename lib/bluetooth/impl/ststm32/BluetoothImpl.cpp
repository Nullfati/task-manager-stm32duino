#include "BluetoothImpl.h"

namespace bluetooth_impl
{

BluetoothImpl::BluetoothImpl(int rx, int tx, int serialSpeed)
    : m_serial(rx, tx)
{
    m_serial.begin(serialSpeed);
    m_serial.flush();
    // TODO: print it only in debug
    m_serial.println("Begin Serial");
    m_serial.println("RX pin " + String(rx));
    m_serial.println("TX pin " + String(tx));
    m_serial.println("Serial speed " + String(serialSpeed));
    m_serial.println("Serial TX buffer size " + String(SERIAL_TX_BUFFER_SIZE));
    m_serial.println("Serial RX buffer size " + String(SERIAL_RX_BUFFER_SIZE));
    m_serial.flush();
}

bluetooth::BluetoothResult BluetoothImpl::readData(size_t sizeOfJson)
{
    using namespace bluetooth;
    using namespace ArduinoJson;

    BluetoothResult result(sizeOfJson);

    if (m_serial.available() <= 0) {
        result.error = BluetoothError::NoDataAvailable;
        return result;
    }

    String inputString = m_serial.readStringUntil('\n');

    bool isStringEmpty = inputString.length() == 0;
    if (isStringEmpty) {
        result.error = BluetoothError::NoDataAvailable;
        return result;
    }

    result.deserializationError = deserializeJson(result.data, inputString);

    if (result.deserializationError != ArduinoJson::DeserializationError::Code::Ok) {
        result.error = BluetoothError::DeserializationError;
        return result;
    }

    result.error = BluetoothError::NoError;
    m_serial.flush();
    return result;
}

void BluetoothImpl::writeData(const String &data)
{
    if (data.length() != 0) {
        m_serial.println(data);
    }
    m_serial.flush();
}

void BluetoothImpl::writeData(ArduinoJson::JsonObjectConst jsonObject)
{
    ArduinoJson::serializeJson(jsonObject, m_serial);
    m_serial.flush();
}

void BluetoothImpl::writeData(const ArduinoJson::JsonDocument &jsonDocument)
{
    ArduinoJson::serializeJson(jsonDocument, m_serial);
    m_serial.flush();
}

}
