#include "BluetoothRead.h"

#include <BluetoothGlobal.hpp>
#include <TaskManager.h>
#include <TaskBuilder.h>

#include "Constants.h"

BluetoothRead::BluetoothRead(CountOfExecute countOfExecute /*= CountOfExecute::Forever*/)
    : Task(0, true, TaskType::Service, countOfExecute)
{
}

void BluetoothRead::run()
{
    using namespace bluetooth;
    using namespace convertors;

    auto result = BluetoothGlobal::getInstance().readDataDefault();

    if (result.error != BluetoothError::NoError) {
        // TODO: process errors
        return;
    }

    const char *command = result.data[json::command];

    auto taskInput = TaskBuilder::createTaskInput(command, result.data[json::command_input]);
    auto task = TaskBuilder::createTaskFromStr(command, result.data[json::count_of_execute], taskInput);

    task_manager::TaskManager::getInstance().addTask(std::unique_ptr<Task>(task));
}
