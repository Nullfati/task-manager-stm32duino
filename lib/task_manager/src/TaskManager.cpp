#include "TaskManager.h"

#include <Task.hpp>

#include <project_config.h>

namespace
{

bool isCorrectTask(Task *task, TaskType taskType)
{
    if (task->taskType() != taskType) {
        return false;
    }

    // Check is count of execute in correct range (! < -2)
    bool isCorrectCountOfExecute =
        task->countOfExecute() <= CountOfExecute::Zero && task->countOfExecute() != CountOfExecute::Forever;
    if (isCorrectCountOfExecute) {
        return false;
    }

    return true;
}

}

namespace task_manager
{

TaskManager::TaskManager(int countOfTasks)
    : m_countOfTasks(countOfTasks)
{

}

TaskManager::~TaskManager()
{
}

TaskManager &TaskManager::getInstance(int countOfTasks)
{
    static TaskManager instance(countOfTasks);
    return instance;
}

bool TaskManager::addTask(std::unique_ptr<Task> task)
{
    if (task == nullptr) {
        return false;
    }

    if (!isCorrectTask(task.get(), TaskType::Normal)) {
        return false;
    }

    bool isQueueFull = m_queueTasks.size() == m_countOfTasks;
    if (isQueueFull) {
        return false;
    }

    m_queueTasks.add(task.release());
    return true;
}

bool TaskManager::addServiceTask(std::unique_ptr<Task> task)
{
    if (task == nullptr) {
        return false;
    }

    if (!isCorrectTask(task.get(), TaskType::Service)) {
        return false;
    }

    bool isQueueFull = m_queueTasks.size() == m_countOfTasks;
    if (isQueueFull) {
        return false;
    }

    m_queueServiceTasks.add(task.release());
    return true;
}

void TaskManager::execute()
{
    for (int i = m_queueTasks.size(); i > 0; --i) {
        digitalWrite(LED_BUILTIN, HIGH);

        Task *task = m_queueTasks.shift();

        task->run();

        if (task->countOfExecute() != CountOfExecute::Forever) {
            --(*task);
        }

        if (task->countOfExecute() == CountOfExecute::Zero) {
            delete task;
        }
        else {
            m_queueTasks.add(task);
        }

        digitalWrite(LED_BUILTIN, LOW);
    }
}

void TaskManager::executeService()
{
    for (int i = m_queueServiceTasks.size(); i > 0; --i) {
        digitalWrite(LED_BUILTIN, HIGH);

        Task *task = m_queueServiceTasks.shift();

        task->run();

        if (task->countOfExecute() != CountOfExecute::Forever) {
            --(*task);
        }

        if (task->countOfExecute() == CountOfExecute::Zero) {
            delete task;
        }
        else {
            m_queueServiceTasks.add(task);
        }

        digitalWrite(LED_BUILTIN, LOW);
    }
}

}
