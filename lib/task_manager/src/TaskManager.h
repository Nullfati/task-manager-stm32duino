#pragma once

#include <memory>

#include <LinkedList.h>

#include <project_config.h>

class Task;

namespace task_manager
{

class TaskManager
{
private:
    explicit TaskManager(int countOfTasks);

    int m_countOfTasks;
    LinkedList<Task *> m_queueTasks;
    LinkedList<Task *> m_queueServiceTasks;

public:
    TaskManager() = delete;
    void operator=(const TaskManager &) = delete;

    ~TaskManager();

    static TaskManager &getInstance(int countOfTasks = project_cfg::task_manager_task_count);

    bool addTask(std::unique_ptr<Task> task);
    bool addServiceTask(std::unique_ptr<Task>  task);
    void execute();
    void executeService();
};

}
