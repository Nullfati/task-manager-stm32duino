Import('env')
from os.path import join, realpath

env.Append(CPPPATH=[realpath(join("targets", env.get("BOARD")))])
env.Replace(SRC_FILTER=["+<*>", "-<targets/>", "+<targets/%s>" % env.get("BOARD")])
