#include "SetValueInEeprom.h"

#include <EEPROM.h>

#include <BluetoothGlobal.hpp>

bool SetValueInEepromInput::fillData(ArduinoJson::JsonObjectConst json)
{
    this->address = json["address"].as<int>();
    this->value = json["value"].as<uint8_t>();

    // TODO: error processing
    return true;
}

SetValueInEeprom::SetValueInEeprom(CountOfExecute countOfExecute, TaskInput *taskInput)
    : Task(0, true, TaskType::Normal, countOfExecute, taskInput)
{
}

void SetValueInEeprom::run()
{
    const auto convertedTaskInput = castAndGetTaskInput<SetValueInEepromInput>();

    EEPROM.write(
        convertedTaskInput->address,
        convertedTaskInput->value);
    EEPROM.commit();

    const size_t capacity = JSON_OBJECT_SIZE(5);
    DynamicJsonDocument doc(capacity);
    doc["command"] = "SetValueInEeprom";
    doc["command_output"]["status"] = "Success";

    bluetooth::BluetoothGlobal::getInstance().writeData(doc);
}
