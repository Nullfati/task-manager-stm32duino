#include "ReadValueFromEeprom.h"

#include <EEPROM.h>

#include <BluetoothGlobal.hpp>

bool ReadValueFromEepromInput::fillData(ArduinoJson::JsonObjectConst json)
{
    this->address = json["address"].as<int>();

    // TODO: error processing
    return true;
}

ReadValueFromEeprom::ReadValueFromEeprom(CountOfExecute countOfExecute, TaskInput *taskInput)
    : Task(0, true, TaskType::Normal, countOfExecute, taskInput)
{
}

void ReadValueFromEeprom::run()
{
    using namespace bluetooth;

    const auto convertedTaskInput = castAndGetTaskInput<ReadValueFromEepromInput>();

    auto result = EEPROM.read(convertedTaskInput->address);

    const size_t capacity = JSON_OBJECT_SIZE(5);
    DynamicJsonDocument doc(capacity);
    doc["command"] = "ReadValueFromEeprom";
    doc["command_output"]["value"] = result;

    BluetoothGlobal::getInstance().writeData(doc);
}
