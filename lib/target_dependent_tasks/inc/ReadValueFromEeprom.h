#pragma once

#include <Task.hpp>

struct ReadValueFromEepromInput : public TaskInput
{
	int address;

	bool fillData(ArduinoJson::JsonObjectConst json) override;
};

class ReadValueFromEeprom : public Task
{
public:
	ReadValueFromEeprom(CountOfExecute countOfExecute, TaskInput *taskInput);

	void run() override;
};
