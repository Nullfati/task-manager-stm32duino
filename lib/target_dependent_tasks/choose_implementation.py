import os

Import('env')

platform = env.get("PIOPLATFORM")
if not os.path.exists(os.path.join("impl", platform)):
    raise Exception("No implementation for {} platform, path to file: {}".format(platform, os.path.join("impl", platform)))

env.Append(CPPPATH=[os.path.realpath(os.path.join("impl", platform))])
env.Replace(SRC_FILTER=["+<*>", "-<impl*>", "+<impl/%s/*>" % platform])
env.Replace(SRC_DIR=[os.path.realpath(os.path.join("impl", platform))])
