import os
import json

Import('env')

config_file = env.get("CMAKE_CONFIG_FILE")

if not os.path.exists(config_file):
    raise Exception("No config file: " + config_file)

with open(config_file) as json_file:
    data = json.load(json_file)
    env.Append(
        CPPDEFINES=[
            ("SERIAL_RX_BUFFER_SIZE", data['SERIAL_RX_BUFFER_SIZE']),
            ("SERIAL_TX_BUFFER_SIZE", data['SERIAL_TX_BUFFER_SIZE'])
        ]
    )
