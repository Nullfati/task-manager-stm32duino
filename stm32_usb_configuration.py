Import('env')

env.Append(
    CPPDEFINES=[
        "PIO_FRAMEWORK_ARDUINO_ENABLE_CDC",
        "PIO_FRAMEWORK_ARDUINO_USB_FULLSPEED",
        "USBCON",
        ("USBD_VID", "0x0483"),
        ("USBD_PID", "0x5740"),
        ("USB_MANUFACTURER", "Unknown"),
        ("USB_PRODUCT", "\"%s\"" % env.get("BOARD").upper()),
        "HAL_PCD_MODULE_ENABLED"
    ]
)
